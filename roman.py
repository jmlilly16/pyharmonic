def roman(number,capital=True):	#roman numeral object; aids in printing roman numerals to the screen
	lowercase = ["i","v","x","l","c","d","m"]
	uppercase = ["I","V","X","L","C","D","M"]
	letters = []
	result = ""
	if capital:
		letters = uppercase
	else:
		letters = lowercase
	remainder = number
	thous, remainder = remainder / 1000, remainder % 1000
	for i in range(0,thous):
		result += letters[6]
	if remainder / 100 == 9:
		if remainder / 10 % 10 is not 9:
			result += letters[4]+letters[6]
			remainder %= 100
		else:
			if remainder % 10 is not 9:
				result += letters[2]+letters[6]
				remainder %= 10
			else:
				result += letters[0]+letters[6]
				return result
	if remainder >= 500:
		remainder -= 500
		result += letters[5]
	if remainder / 100 is 4:
		if remainder / 10 % 10 is not 9:
			result += letters[4]+letters[5]
			remainder %= 100
		else:
			if remainder % 10 is not 9:
				result += letters[2]+letters[5]
				remainder %= 10
			else:
				result += letters[0]+letters[6]
				return result
	hundreds,remainder = remainder / 100, remainder % 100
	for i in range(0,hundreds):
		result += letters[4]
	if remainder / 10 is 9:
		if remainder % 10 is not 9:
			result += letters[2]+letters[4]
			remainder %= 10
		else:
			result += letters[0]+letters[4]
			return result
	if remainder >= 50:
		remainder -= 50
		result += letters[3]
	if remainder / 10 is 4:
		if remainder % 10 is not 9:
			result += letters[2]+letters[3]
			remainder %= 10
		else:
			result += letters[0]+letters[3]
			return result
	tens,remainder = remainder/10,remainder%10
	for i in range(0,tens):
		result += letters[2]
	if remainder is 9:
		result += letters[0]+letters[2]
		return result
	if remainder >= 5:
		result += letters[1]
		remainder -= 5
	if remainder is 4:
		result += letters[0]+letters[1]
		return result
	for i in range(0,remainder):
		result += letters[0]
	return result
