from tuplicate import tuplicate
from roman import roman

notes = ['A','Bb','B','C','C#','D','Eb','E','F','F#','G','Ab']
#a better model for naming notes and keys should come eventually, but for now i'm just going to use this basic one
#in which the twelve pitches are named according to their most "relaxed" enharmonic name.

A, Bb, C, Cs, D, Eb, F, Fs, G, Ab = 'A','Bb','C','C#','D','Eb','F','F#','G','Ab'

class Degree:
	def __init__(self,number,accidental=0):
		self.number = number
		self.accidental = accidental
	def __eq__(self,other):
		return self.number == other.number and self.accidental == other.accidental
	def __ne__(self,other):
		return not self.__eq__(other)
	def __hash__(self):
		return hash((self.number,self.accidental))
	def __str__(self):
		return self.number

class Scale:
	#accepts a set of scale degrees, which are stored in a frozenset to become the scale.
	#This version of a scale object stores the degrees that make up a scale.
	#Another version might store intervals instead, but that'd come later. (e.g. "whole, half, whole,...")
	def __init__(self,tones):
		self.tones = tones	#interval in semitones above 0 for each scale degree
	def __eq__(self,other):
		return self.tones == other.tones
	def __ne__(self,other):
		return not self.__eq__(other)
	def __hash__(self):
		return hash(self.tones)
	def __str__(self):
		for scale in scaleNames.keys():
			if self.__dict__ == scale.__dict__:
				return scaleNames[scale]
		else:
			return str(degrees)
	def types(self):
		result = []
		modbase = len(self.tones)
		for i in range(0,modbase):
			root = self.tones[i%modbase]
			third = (self.tones[(i+2)%modbase] - root) % 12
			fifth = (self.tones[(i+4)%modbase] - root) % 12
			result.append(ChordType((0,third,fifth))) #0 instead of root
		return result
	def functions(self):
		types = self.types()
		result = []
		for i in range(0,len(types)):
			result.append(Function(i+1,types[i]))
		return result
	def degreeTone(self,degree):
		if degree.__class__ is not Degree:
			degree = Degree(degree)
		return (self.tones[degree.number%len(self.tones)-1]+degree.accidental) % 12
	def degreeFunction(self,degree):
		if degree.__class__ is not Degree:
			degree = Degree(degree)
		return Function(degree,self.types()[(degree.number%len(self.tones))-1])

class ChordType:
	#accepts a set of chord degrees - similar to a scale - representing the intervals above root that are in the chord
	#for example, the major type defined below is initialized as ChordType([0,4,7])
	#an actual Chord object combines a ChordType with a root number to form, for example, a 4-major or 2-diminished chord.
	def __init__(self,intervals):
		self.intervals = frozenset(intervals)
	def __eq__(self,other):
		return self.intervals == other.intervals
	def __ne__(self,other):
		return not self.__eq__(other)
	def __hash__(self):
		return hash(self.intervals)
	def __str__(self):
		for type in chordNames:
			if self.intervals == type.intervals:
				return chordNames[type]
		else:
			return str(self.intervals)

class Chord:
	def __init__(self,root,type,inversion=0):
		#'root' can be either a tone number or a note name
		if root in notes:
			self.root = root	#Note ('Bb', e.g.) of the root of this chord
			self.rootnumber = notes.index(root) #absolute number of the tonic (e.g. 'Bb' == 1)
		else:
			self.root = notes[root]
			self.rootnumber = root
		self.type = type	#Chord type=, e.g. "Major"
		self.inversion = inversion
        def __eq__(self,other):
		return self.root == other.root and self.type == other.type
        def __ne__(self,other):
		return not self.__eq__(other)
        def __hash__(self):
		return hash((self.root,self.type))
	def __str__(self):
		return str(self.root)+'-'+str(self.type)
	def notes(self):
		result = []
		for i in self.type.intervals:
			result.append(notes[(i+self.rootnumber)%12])
		i = self.inversion
		while i > 0:
			result = result[1:] + result[:1]
			i-=1
		return result
	def inKey(self,key):
		return self in key.triads()
	def functionIn(self,key):
		return key.functionOf(self)
	def invert(self,inversions=1):
		return Chord(self.root,self.type,(self.inversion+inversions)%len(self.type.intervals))
			

class Function:
	def __init__(self,degree,type,inversion=0):
		if degree.__class__ is not Degree:
			degree = Degree(degree)
		self.degree = degree
		self.type = type
		self.inversion = inversion
	def __eq__(self,other):
		return self.degree == other.degree and self.type == other.type
	def __ne__(self,other):
		return not self.__eq__(other)
	def __hash__(self):
		return hash((self.degree,self.type))
	def __str__(self):
		major = True
		if self.type in (minorTriad,diminishedTriad,minorSeventh,halfDiminishedSeventh,diminishedSeventh,minorMajorSeventh):
			major = False
		result = roman(self.degree.number,major) + '-' + str(self.type)
		return result
		#TODO: eventually this needs more nuance

class Key:
	#accepts a note name as tonic and a Scale
	def __init__(self,tonic,scale):
		self.tonic = tonic #Note representation (e.g. 'C#') of the tonic of this key
		self.scale = scale #scale used by this key (e.g. majorDiatonic)
		self.rootnumber = notes.index(tonic) #absolute number of the tonic (e.g. 'C#' == 4)
	def __eq__(self,other):
		return self.tonic == other.tonic and self.scale == other.scale
	def __ne__(self,other):
		return not self.__eq__(other)
	def __hash__(self):
		return hash((self.tonic,self.scale))
	def __str__(self):
		return str(self.nameNotes())
	def notes(self):
		result = []
		for i in self.scale.tones:
			result.append(notes[(i+self.rootnumber)%12])
		return result
	def nameDegree(self,degree):
		if degree.__class__ is not Degree:
			degree = Degree(degree)
		return notes[(self.scale.degreeTone(degree)+self.rootnumber) % 12]
	def degreeOfNote(self,note):
		notes = self.notes()
		if note not in notes:
			return None
		return Degree(notes.index(note)+1)	#convention: degrees are never 0.
	def triads(self):
		types = self.scale.types()
		result = []
		for note,type in tuplicate([self.notes(),types]):
			result.append(Chord(note,type))
		return result
	def functionOf(self,chord):
		if chord not in self.triads():
			return None
		degree = self.degreeOfNote(chord.root)
		if degree is None:
			return None
		return Function(degree,chord.type)
	def nameFunction(self,func):
		if func not in self.scale.functions():
			return None
		return Chord(self.nameDegree(func.degree),func.type)
	def degreeChord(self,degree):
		if degree.__class__ is not Degree:
			degree = Degree(degree)
		return self.nameFunction(self.scale.degreeFunction(degree))

def chordFrom(notenames):
	intervals = []
	for note in notenames:
		intervals.append(notes.index(note))
	intervals = reduceSet(intervals)
	minInversion = 0
	minDistance = sum(intervals)
	minIntervals = list(intervals)
	for i in range(0,len(intervals)):
		intervals = rotateSet(intervals)
		if sum(intervals) < minDistance:
			minDistance = sum(intervals)
			minIntervals = intervals
			minInversion = i #not actually the inversion number yet, just the iteration on which the minimum inversion was found
	type = ChordType(minIntervals)
	inversion = len(intervals) - minInversion
	#return Chord(notes[minIntervals[0]],type)

def reduceSet(intervalset):
	#sorts and reduces a set of intervals mod 12 and subtracts n from all elements so that the first element is 0
	intervalset = sorted(intervalset)
	base = intervalset[0]
	intervalset = map(lambda x: x-base,intervalset)
	intervalset = map(lambda x: x%12,intervalset)
	return intervalset

def rotateSet(intervalset):
	#makes an 'inversion' of an interval set by adding 12 to the first element and passing the set to reduceSet()
	#note that the interval set should already have been reduced before being passed to this function
	intervalset[0] += 12
	return reduceSet(intervalset)

#basic scale types
majorDiatonic = Scale((0,2,4,5,7,9,11))
harmonicMinorDiatonic = Scale((0,2,3,5,7,8,11))
minorDiatonic = harmonicMinorDiatonic
naturalMinorDiatonic = Scale((0,2,3,5,7,8,10))
octatonicA = Scale((0,1,3,4,6,7,9,10))
octatonicB = Scale((0,2,3,5,6,8,9,11))
wholeTone = Scale((0,2,4,6,8,10))

scaleNames = {majorDiatonic: "Major", harmonicMinorDiatonic: "Harmonic Minor", naturalMinorDiatonic: "Natural Minor", octatonicA: "Octatonic (Half)", octatonicB: "Octatonic (Whole)", wholeTone: "Whole Tone"}

#basic chord types
majorTriad = ChordType((0,4,7))
minorTriad = ChordType((0,3,7))
diminishedTriad = ChordType((0,3,6))
augmentedTriad = ChordType((0,4,8))
dominantSeventh = ChordType((0,4,7,10))
minorSeventh = ChordType((0,3,7,10))
diminishedSeventh = ChordType((0,3,6,))
halfDiminishedSeventh = ChordType((0,3,6,10))
minorMajorSeventh = ChordType((0,3,7,11))
majorSeventh = ChordType((0,4,7,11))

chordNames = {majorTriad: "Major", minorTriad: "Minor", diminishedTriad: "Diminished", augmentedTriad: "Augmented", dominantSeventh: "Dominant 7th", minorSeventh: "Minor Seventh", diminishedSeventh: "Diminished Seventh", halfDiminishedSeventh: "Half-Diminished Seventh", minorMajorSeventh: "Minor-Major Seventh", majorSeventh: "Major Seventh"}

#accidental mods
DIMINISHED = -2
FLAT = -1
NATURAL = 0
SHARP = 1
AUGMENTED = 2

##testing stuff
FMajor = Key(F,majorDiatonic)
