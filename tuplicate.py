def tuplicate(lists):
	#Accepts a list of lists. Returns a list of tuples each of length len(lists) where the nth tuple in the list contains the nth element of each of the argument lists.
	result = []
	for i in range(0,min(map(len,lists))):
		entries = []
		for j in range(0,len(lists)):
			entries.append(lists[j][i])
		result.append(tuple(entries))
	return result
